#!/usr/bin/python3
import os
import json
import random
import traceback

import users

import re
import tweepy
import discord
from discord.ext import commands
from tweepyauth import auto_authenticate


USR_MINIFY_PROPERTIES = {
	'Name': 'name',
	'@': 'screen_name',
	'Twitter ID': 'id_str',
	'Bio': 'description',
	'Location': 'location',
	'Account created at': 'created_at',
	'Followers': 'followers_count',
	'Following': 'friends_count',
}

USR_MINIFY_PROPERTIES_NOBIO = USR_MINIFY_PROPERTIES.copy()
del USR_MINIFY_PROPERTIES_NOBIO['Bio']

FRMT_RELATIONS_PROPERTIES = {
	'Blocked them': 'blocking',
	'Blocked me': 'blocked_by',
	'Following them': 'following',
	'Follows me': 'followed_by',
	#'Requested to follow them': 'following_requested',
	#'Requested to follow me': 'following_received',
}

FRMT_STATUS = {
	'Created at':'created_at',
	'ID':'id',
	'Tweet text':'text',
	'Retweets':'retweet_count',
	'Likes':'favorite_count',
	# 'Replies':'reply_count',
	# 'Q Retweets':'qoute_count',
}

client = discord.Client()
bot = commands.Bot(command_prefix=['&'], case_insensitive=True)

def compact(outputs):
	return '\n```py\n'+'\n'.join(list(outputs))+'\n```'

def user_minify(usr, properties=USR_MINIFY_PROPERTIES_NOBIO):
	rout = []
	for name,key in properties.items():
		rout.append(f'{name:>25}: {getattr(usr,key)}')
	return '\n'.join(rout)

def dict_user_minify(a):
	rout = []
	for name,val in a.items():
		rout.append(f'{name:>25}: {val}')
	return '\n'.join(rout)

def format_relations(api, user_id):
	me = twit.get(api).show_friendship(target_id=user_id)[0]
	return user_minify(me, properties=FRMT_RELATIONS_PROPERTIES)

def format_status(status):
	# print(vars(status))
	return user_minify(status, properties=FRMT_STATUS)

@bot.event
async def on_connect():
	print('Connected to discord')

@bot.event
async def on_ready():
	print('Discord ready')

# @bot.command(brief='Post to all accounts')
# async def post(ctx, *, post):
# 	if str(ctx.message.author.id) in users.administrators:
# 		twit.post(post)

@bot.command(alias=['delete','d','r'],name='remove',brief='Removes a twitter post')
async def remove(ctx,*tweets):
	scs = {True:'Tweet deleted', False:'Tweet __not__ deleted'}
	if str(ctx.message.author.id) in users.administrators:
		for tweet in tweets:
			try:
				regex = '[0-9]'*19
				print(regex,',',tweet)
				tweet_id = re.findall(regex,tweet)[0] #1361723833074475009
				print(tweet_id)
				print('found id',tweet_id)
				await handle_discordupdate(ctx, scs, twit.delete(tweet_id))
			except Exception as e:
				traceback.print_exc()
				await ctx.channel.send(f'An error occured: {e}')
				# raise e
		# print(f'[{ctx.message.guild}] (#{ctx.chQYRJZFannel}) {ctx.message.author}: {ctx.message.clean_content}')
		# try:
		# 	for tweet in tweets:
		# 		try:
		# 			tweet_id = tweet.split('/')[-1]
		# 			success,outputs = twit.delete(tweet_id)
		# 			if success:
		# 				await ctx.channel.send('Tweet deleted'+compact(outputs))
		# 			else:
		# 				await ctx.channel.send('Tweet not deleted'+compact(outputs))
		# 		except Exception as e:
		# 			await ctx.channel.send(f'An error occured: {e}')
		# except Exception as e:
		# 	await ctx.channel.send(f'An error occured: {e}')

async def handle_discordupdate(ctx, scs, outputs, only_output_on_err=False):
	def user_handle(from_, items):
		out = []
		if not type(items) in {list, tuple, set}:
			items = [items]
		for output in items:
			print(output)
			print(type(output))
			if type(output) == tweepy.User:
				out.append(' '*15+' == User overview  ==')
				out.append(user_minify(output))
				out.append(' ')
				out.append(' '*15+' ==   Relations    ==')
				out.append(format_relations(from_, output.id))
			elif type(output) == tweepy.Status:
				out.append(' '*15+' == Tweet overview ==')	
				out.append(format_status(output))
			elif isinstance(output,Exception):
				out.append(' '*15+' == Error overview ==')
				out.append(dict_user_minify(output.args[0][0]))
				# out.append(str(dir(output)))
				# out.append(str(vars(output)))
				# out.append(str(output.args))
				# out.append(str(output.response))
				# out.append(str(output.reason))
				# out.append(str(output.reason[0]))
				# out.append(str(type(output.reason[0])))
			else:
				out.append(str(output))
		return '\n'.join(out)

	rout = []
	for user in outputs:
		me = f'**{user}**: {scs[outputs[user]["success"]]}'
		if only_output_on_err:
			if not outputs[user]["success"]:
				# me += '\n```py\n'+user_handle(user, outputs[user]['out'])+'```'
				me += '\n```py\n'+user_handle(user, outputs[user]['out'])+'```'
		else:
			me += '\n```py\n'+user_handle(user, outputs[user]['out'])+'```'
		rout.append(me)
	
	out = '\n'.join(rout)
	
	if len(out) >= 2000:
		print("TOO LONG: ",len(out))
		await ctx.channel.send(out[:1950]+"...```\n **NOTE:** output was >2k characters")
	else:
		await ctx.channel.send(out)

@bot.command(brief='post to all accounts')
async def tweet(ctx, post):
	scs = {True: 'Message weeted', False:'Message __not__ tweeted'}
	if type(post) == list:
		post = ' '.join(post)
	if str(ctx.message.author.id) in users.administrators:
		try:
			await handle_discordupdate(ctx, scs, twit.tweet(post))
		except Exception as e:
			traceback.print_exc()
			await ctx.channel.send(f'An error occured: {e}')

@bot.command(brief='Show overview of a user from the perspective of the apis')
async def user(ctx, *userss):
	if str(ctx.message.author.id) in users.administrators:
		for user in userss:
			scs = {True: f'User {user} fetched', False:f'User {user} __not__ fetched'}
			try:
				await handle_discordupdate(ctx, scs, twit.get_user(user))
			except Exception as e:
				traceback.print_exc()
				await ctx.channel.send(f'An error occured: {e}')

@bot.command(brief='unblock a twitter user')
async def unblock(ctx,*userss):
	if str(ctx.message.author.id) in users.administrators:
		for user in userss:
			scs = {True: f'User {user} unblocked', False: f'User {user} __not__ unblocked'}
			try:
				await handle_discordupdate(ctx, scs, twit.unblock(screen_name=user))
			except Exception as e:
				traceback.print_exc()
				await ctx.channel.send(f'An error occured: {e}')

@bot.command(brief='Block a twitter user')
async def block(ctx,*userss):
	if str(ctx.message.author.id) in users.administrators:
		for user in userss:
			scs = {True: f'User {user} blocked', False: f'User {user} __not__ blocked'}
			try:
				await handle_discordupdate(ctx, scs, twit.block(screen_name=user))
			except Exception as e:
				traceback.print_exc()
				await ctx.channel.send(f'An error occured: {e}')

# @bot.command(brief='run an api call directly',name='eval')
# async def _eval(ctx,*userss):
# 	if str(ctx.message.author.id) in users.administrators:
# 		for user in userss:
# 			try:
# 				success,outputs = twit.do()
# 				if success:
# 					await ctx.channel.send(f'User {user} blocked'+compact(outputs))
# 				else:
# 					await ctx.channel.send(f'User {user} not blocked'+compact(outputs))
# 			except Exception as e:
# 				await ctx.channel.send(f'An error occured: {e}')
# 				raise e

class multitwit():
	def __init__(self,filelist,keyfile='twitter_keys.txt',directory='./accounts/'):
		self.apis = {}
		for item in filelist:
			try:
				newapi = auto_authenticate(keyfile=keyfile,tokenfile=directory+item)
				self.apis[str(newapi.me().screen_name)] = newapi
			except Exception as e:
				print(e)
				# raise(e)

	def doSimple(self, action, *args, **kwargs):
		return getattr(random.choice(list(self.apis.values())), action)(*args, **kwargs)

	def doSpecific(self, target, action, *args, **kwargs):
		try:
			output = getattr(self.apis[target],action)(*args, **kwargs)
			outputs = {'success':False,'out':output}
		except Exception as e:
			traceback.print_exc()
			# raise e
			outputs = {'success':False,'out':str(e),'exception':str(e)}
		return outputs
	
	def get_api(self, key):
		return self.apis[key]
	get = get_api


	def do(self,action,*args,**kwargs):
		outputs = {}
		for item in self.apis:
			try:
				output = getattr(self.apis[item],action)(*args, **kwargs)
				outputs[item] = {'success':True,'out':output}
			except Exception as e:
				# print(item,e)
				print('\n'.join([ '    '+ a for a in traceback.format_exc().split('\n') ]))
				# raise e
				outputs[item] = {'success':False,'out':e}
		return outputs
	
	def show_friendship(self, *args, **kwargs):
		return self.do('show_friendship', *args,**kwargs)
	
	# def isblocked(self, *args, **kwargs):
	# 	return self.do('')
	# 	tweepy.User.

	def tweet(self, *args, **kwargs):
		return self.do('update_status', *args, **kwargs)

	def get_user(self, *args, **kwargs):
		return self.do('get_user', *args, **kwargs)

	def block(self,*args,**kwargs):
		# list(self.apis.values())[0].get_user(**kwargs)
		out = self.do('create_block',*args,**kwargs)
		return out

	def unblock(self, *args, **kwargs):
		return self.do('destroy_block', *args, **kwargs)

	def post(self, *args, **kwargs):
		return self.do('update_status', *args, **kwargs)

	def delete(self,tweetID):
		return self.do('destroy_status',tweetID)
		# success = False
		# outputs = set()
		# for item in self.apis:
		# 	try:
		# 		self.apis[item].destroy_status(tweetID)
		# 		success = True
		# 	except Exception as e:
		# 		print(item,e)
		# 		outputs.add(str(item)+": "+str(e))
		# return success,outputs

if __name__ == '__main__':
	global twit
	files = os.listdir('./accounts')
	print(len(files),'registered users')
	if len(files) == 0:
		import registeracc
		registeracc.registeracc()
		files = os.listdir('./accounts')
	twit = multitwit(files)
	# except tweepy.error.RateLimitError:
	# 	print(f'Twitter ratelimited!, waiting for 15 min')
	# 	time.sleep(15*60)

	with open('discord_token.txt','r') as f:
		discord_token = f.read()
	bot.run(discord_token)
