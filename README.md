 
# disctwittermod
small and pretty garbage discord bot to moderate multiple twitter accounts, with stunning features such as:
- deleting twitter posts from any of the registered twitter acocunts
- blocking & unblocking users on all accounts at once
- bad & messy code
- posting a tweet to all accounts at once
- viewing twitter users from the perspective of the various registered twitter accounts, with block data and a minified user profile

## ok
Mostly just putting this here because i used it for moderating instances of the now discontinued [TootbotX](https://gitlab.com/mocchapi/tootbotX), and since *thats* open source i thought i might as well release the additional tooling. 

The code is legitimately terrible and filled with bullshit, so i dont recommend you actually use this but it might be somewhat helpful if youre trying to implement something similar. I think it goes without saying that this is not under active development and no support will be given, similar to tootbotx.

## setup
if for some god damn reason you feel like spinning up a copy anyways, youll need to do some manual setup since again, the code is garbage and mostly intended as a quick internal tool.

Anyways do the following lol (also no gaurantees that thisll work on anything except linux)
 1. Make a folder named "accounts" in the same directory as the one you're running the script from.
 2. Make a file named "discord_token.txt" containing your discord bot token in the same directory as the one you're running the script from.
 3. Im not *sure* but im fairly sure a "twitter_tokens.txt" will be automatically created on first run, which will ask for your twitter api tokens. if not, sorry, but i have no clue how this works anymore.
 3. Add your own discord user ID to the list in `users.py` as a **string**. If you want to let someone else moderate too, you can add their IDs anytime (as long as you reboot the bot after)
 4. Run `registeracc.py` and follow the setup. When it asks for a name i think its fine to just give it anything.
 5. Run `main.py` to start the bot.